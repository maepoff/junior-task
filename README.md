
##Тестовое задание для Junior DevOps##


При помощи Docker compose запускаются следующие сервисы:  
- Веб сервер Nginx  
- Экспортер nginx https://github.com/nginxinc/nginx-prometheus-exporter  
- Prometheus (https://prometheus.io/)  
- ELK стек (https://www.elastic.co/what-is/elk-stack)  


Для запуска:  
git clone https://gitlab.com/maepoff/junior-task.git  
cd junior-task/  
COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up -d


Посмотреть метрики nginx можно здесь:  
http://127.0.0.1:8002

Логи nginx можно посмотреть в разделе Observability -> Logs  
http://127.0.0.1:8001

Логин: elastic  
Пароль: passwd
